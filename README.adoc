= Some Work in Progress
:created: 2023-09-24
:modified: 2023-09-25 14:47
:swip-domain: someworkinprogress.org
:swip-display-name: Some Work in Progress
:swip-proj: some-work-in-progress
:url-gitlab: https://gitlab.com/{swip-proj}
:swip-pages: https://{swip-proj}.gitlab.io
:url-tech: {url-gitlab}/tech
:url-www: {url-gitlab}/{swip-proj}.gitlab.io
:url-swip: https://{swip-domain}
:icons: font
:toc: left

This repository contains content and code for the {swip-display-name} project website.

* The website is located at {swip-pages} and {url-swip}
* Sources are canonically located at {url-www}

On-topic material includes:

* What is SWiP [going to be]? Setting the tone and providing context with intentions, principles, etc.
* What areas of effort are most interesting [first/now]?
* Roadmap
* Ways to participate
* What has happened so far?
